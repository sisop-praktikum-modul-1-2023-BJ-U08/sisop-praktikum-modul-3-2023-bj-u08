#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define ROW1 4
#define COL2 5

int factorial(unsigned long long int n) 
{
    unsigned long long int fact = 1;
    for (unsigned long long int i = 1; i <= n; i++) 
    {
        fact *= i;
    }
    return fact;
}

int main() 
{
    key_t key = ftok("shared_memory", 15);
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666 | IPC_CREAT);
    int *shm_ptr = (int*) shmat(shmid, NULL, 0);

    while (*shm_ptr == 0) 
    {
        sleep(1);
    }

    unsigned long long int result_matrix[ROW1][COL2];

    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            int sum = 0;
            for (unsigned long long int k = 0; k < COL2; k++) 
            {
                sum += *(shm_ptr + i * COL2 + k) * *(shm_ptr + k * COL2 + j);
            }
            result_matrix[i][j] = sum;
        }
    }

    printf("Result of matrix multiplication:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%d ", result_matrix[i][j]);
        }
        printf("\n");
    }

    unsigned long long int factorial_matrix[ROW1][COL2];

    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            factorial_matrix[i][j] = factorial(*(shm_ptr + i * COL2 + j));
        }
    }

    printf("Factorial of the matrix:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%lld ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    shmdt(shm_ptr);

    return 0;
}
