#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>

#define ROW1 4
#define COL2 5

int calculate_factorial(unsigned long long int n) 
{
    int fact = 1;
    for (unsigned long long int i = 1; i <= n; i++) 
    {
        fact *= i;
    }
    return fact;
}

void *calculate_factorial_thread(void *arg) 
{
    int *result = (int*) arg;

    int factorial_matrix[ROW1][COL2];

    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            factorial_matrix[i][j] = calculate_factorial(result[i * COL2 + j]);
        }
    }

    printf("Factorial of the matrix:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%lld ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    pthread_exit(NULL);
}

int main() 
{

    key_t key = ftok("shared_memory", 15);
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666 | IPC_CREAT);
    int *shm_ptr = (int*) shmat(shmid, NULL, 0);

    while (*shm_ptr == 0) 
    {
        sleep(1);
    }

    printf("Result of matrix multiplication:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%lld ", *(shm_ptr + i * COL2 + j));
        }
        printf("\n");
    }

    pthread_t tid;
    pthread_create(&tid, NULL, calculate_factorial_thread, shm_ptr);

    pthread_join(tid, NULL);

    shmdt(shm_ptr);

    return 0;
}