#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROWS_1 4
#define COLS_1 2
#define ROWS_2 2
#define COLS_2 5

void printMatrix(int rows, int cols, int matrix[][cols]) 
{
    for (int i = 0; i < rows; i++) 
    {
        for (int j = 0; j < cols; j++) 
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void multiplyMatrices(int rows1, int cols1, int mat1[][cols1], int rows2, int cols2, int mat2[][cols2], int res[][cols2]) 
{
    for (int i = 0; i < rows1; i++) 
    {
        for (int j = 0; j < cols2; j++) 
        {
            int sum = 0;
            for (int k = 0; k < cols1; k++) 
            {
                sum += mat1[i][k] * mat2[k][j];
            }
            res[i][j] = sum;
        }
    }
}

int main() 
{
    int matrix1[ROWS_1][COLS_1];
    int matrix2[ROWS_2][COLS_2];
    int result[ROWS_1][COLS_2];

    srand(time(NULL)); 
    for (int i = 0; i < ROWS_1; i++) 
    {
        for (int j = 0; j < COLS_1; j++) 
        {
            matrix1[i][j] = rand() % 5 + 1; 
        }
    }

    srand(time(NULL));
    for (int i = 0; i < ROWS_2; i++) 
    {
        for (int j = 0; j < COLS_2; j++) 
        {
            matrix2[i][j] = rand() % 4 + 1; 
        }
    }

    printf("First matrix:\n");
    printMatrix(ROWS_1, COLS_1, matrix1);

    printf("Second matrix:\n");
    printMatrix(ROWS_2, COLS_2, matrix2);

    multiplyMatrices(ROWS_1, COLS_1, matrix1, ROWS_2, COLS_2, matrix2, result);

    key_t key = ftok("shared_memory", 15);
    int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT);
    int *shmPointer = (int*) shmat(shmid, NULL, 0);

    for (int i = 0; i < ROWS_1; i++) 
    {
        for (int j = 0; j < COLS_2; j++) 
        {
            *(shmPointer + i * COLS_2 + j) = result[i][j];
        }
    }

    printf("Multiplication result: \n");
    printMatrix(ROWS_1, COLS_2, result);

    shmdt(shmPointer);

    return 0;
}