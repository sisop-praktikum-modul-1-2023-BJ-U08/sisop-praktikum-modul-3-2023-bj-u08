/*
! PSEUDO CODE 
? Parent process:
1. Open file
2. Calculate the frequency of occurrence of each letter in the file and save the findings.
3. Make child process
4. Using a pipe, send the result of calculating the frequency of each letter to the child process.
5. Save the compressed result from the child process in a new file.
6. From the compressed file read Huffman tree
7. Read Huffman code and do decompression
8. Calculate the number of bits after compression and compare it to the number of bits before compression.

? Child process:
1. Using a pipe, read the frequency of occurrence of letters from the parent process.
2. Compress files based on the frequency of letters received using the Huffman method.
3. Make a compressed file of the Huffman tree.
4. Convert each letter in the file into a Huffman code and deliver that code to the parent process via a pipe.
5. Using a pipe, send the compressed result to the parent process.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <ctype.h>

#define MAX_TREE_HT 100

/* 
! Struct
*/

typedef struct node {
    char letter;
    int freq;
    struct node *left, *right;
} Node;

typedef struct heap {
    int size;
    int capacity;
    Node **arr;
} minHeap;

typedef struct decodeNode {
    char letter;
    struct decodeNode *left, *right;
} decodeTree;

/* 
! Huffman Algorithm
*/

Node* newNode(char letter, int freq) {
    Node *temp = (Node *)malloc(sizeof(Node));

    temp->left = temp->right = NULL;
    temp->letter = letter;
    temp->freq = freq;

    return temp;
}

decodeTree* newDecodeTree() {
    decodeTree *tree = (decodeTree *)malloc(sizeof(decodeTree));

    tree->left = NULL;
    tree->right = NULL;
    tree->letter = '$';

    return tree;
}

minHeap* createMinHeap(int capacity) {
    minHeap *mHeap = (minHeap *)malloc(sizeof(minHeap));

    mHeap->size = 0;
    mHeap->capacity = capacity;
    mHeap->arr = (Node **)malloc(mHeap->capacity * sizeof(Node *));

    return mHeap;
}

void swapNode(Node **a, Node **b) {
    Node *t = *a;
    *a = *b;
    *b = t;
}

void heapify(minHeap *mHeap, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < mHeap->size && mHeap->arr[left]->freq < mHeap->arr[smallest]->freq) smallest = left;

    if (right < mHeap->size && mHeap->arr[right]->freq < mHeap->arr[smallest]->freq) smallest = right;

    if (smallest != idx) {
        swapNode(&mHeap->arr[smallest], &mHeap->arr[idx]);
        heapify(mHeap, smallest);
    }
}

int isSizeOne(minHeap *mHeap) { return (mHeap->size == 1); }

Node* extractMin(minHeap *mHeap) {
    Node *temp = mHeap->arr[0];
    mHeap->arr[0] = mHeap->arr[mHeap->size - 1];

    --mHeap->size;
    heapify(mHeap, 0);

    return temp;
}

void insertMinHeap(minHeap *mHeap, Node *minHeapNode) {
    ++mHeap->size;
    int i = mHeap->size - 1;

    while (i && minHeapNode->freq < mHeap->arr[(i - 1) / 2]->freq) {
        mHeap->arr[i] = mHeap->arr[(i - 1) / 2];
        i = (i - 1) / 2;
    }

    mHeap->arr[i] = minHeapNode;
}

void buildMinHeap(minHeap *mHeap) {
    int n = mHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i) heapify(mHeap, i);
}

int isLeaf(Node *root) { return !(root->left) && !(root->right); }

minHeap* createAndBuildMinHeap(char item[], int freq[], int size) {
    minHeap *mHeap = createMinHeap(size);

    for (int i = 0; i < size; ++i) mHeap->arr[i] = newNode(item[i], freq[i]);

    mHeap->size = size;
    buildMinHeap(mHeap);

    return mHeap;
}

Node* buildHuffmanTree(char item[], int freq[], int size) {
    Node *left, *right, *top;
    minHeap *mHeap = createAndBuildMinHeap(item, freq, size);

    while (!isSizeOne(mHeap)) {
        left = extractMin(mHeap);
        right = extractMin(mHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(mHeap, top);
    }
    
    return extractMin(mHeap);
}

void saveHuffman(Node *root, char huffmanArr[26][30], int arr[], int n, char ch) {
    if (root -> left) {
        arr[n] = 0;
        saveHuffman(root->left, huffmanArr, arr, n + 1, ch);
    }

    if (root->right) {
        arr[n] = 1;
        saveHuffman(root->right, huffmanArr, arr, n + 1, ch);
    }

    if (isLeaf(root)) {
        if (root->letter == ch) {
            int i;
            for (i = 0; i < n; i++) {
                if (arr[i] == 1 || arr[i] == 0) {
                    huffmanArr[ch - 'A'][i] = arr[i] + '0';
                }

                huffmanArr[ch - 'A'][i + 1] = '\0';
            }
        }
    }
}

void printArray(int arr[], int n, FILE *fp, char ch) {
    int i;
    for (i = 0; i < n; ++i) putc(arr[i] + '0', fp);
}

void printHCodes(Node *root, int arr[], int top, FILE *fp, char ch) {
    if (root->left) {
        arr[top] = 0;
        printHCodes(root->left, arr, top + 1, fp, ch);
    }

    if (root->right) {
        arr[top] = 1;
        printHCodes(root->right, arr, top + 1, fp, ch);
    }

    if (isLeaf(root)) {
        if (root->letter == ch) printArray(arr, top, fp, ch);
    }
}

/* 
! End Huffman Algorithm
*/

/* 
! Decode Function
*/

void decode(char huffmanArr[30][30]) {
    decodeTree *tree = newDecodeTree('$');
    decodeTree *temp;

    for (int i = 0; i < 26; i++) {
        int j = 0;

        temp = tree;

        if (i == 16 || i == 21 || i == 23) {
            continue;
        } else {
            while (1) {
                char ch = huffmanArr[i][j];
                if (ch == '\0') {
                    temp->letter = 'A' + i;
                    break;
                } else if (ch == '0') {
                    if (temp->left == NULL) {
                        decodeTree *leftTemp = newDecodeTree();
                        temp->left = leftTemp;
                    }

                    temp = temp->left;
                } else if (ch == '1') {
                    if (temp -> right == NULL) {
                        decodeTree *rightTemp = newDecodeTree();
                        temp->right = rightTemp;
                    }

                    temp = temp->right;
                }

                j++;
            }
        }
    }

    FILE *encoded = fopen("enc.txt", "r");
    FILE *decoded = fopen("dec.txt", "w");

    decodeTree *solve = tree;
    char ch;

    while ((ch = fgetc(encoded)) != EOF) {
        if (ch == '\n') {
            putc('\n', decoded);
        } else if (ch == ' ') {
            putc(' ', decoded);
        } else {
            if (ch == '1') {
                solve = solve->right; 
            } else if (ch == '0') {
                solve = solve->left;
            }

            if (solve->letter <= 'Z' && solve->letter >= 'A') {
                putc(solve->letter, decoded);
                solve = tree;
            }
        }
    }

}

/* 
! End Decode Function
*/

int main() {
	// ? NOTE: fd[0] = read && fd[1] = write
    int fdFirst[2], fdSecond[2];

    if(pipe(fdFirst) == -1) {
		printf("An error occured when opening the first pipe\n");
		return 1;
	}

	if(pipe(fdSecond) == -1) {
		printf("An error occured when opening the second pipe\n");
		return 2;
	}

    pid_t p;
    p = fork();

    if (p == -1) {
        perror("Failed to create child process");
        exit(EXIT_FAILURE);
    }

    if (p > 0) {
        /* 
		! Parent process
		*/
        int charArr[26] = {0},
            charArrToSend[23] = {0},
            totalBytes = 0;

        //  ? Close read end from pipe
        close(fdFirst[0]);

        // ? Open File
        FILE *fp = fopen("file.txt", "r");

        if (fp == NULL) {
			printf("File tidak ditemukan atau tidak dapat dibuka.\n");
            return 1;
        }

        // ! A
		// ? Count each alphabet and store
        char ch;
        while ((ch = fgetc(fp)) != EOF) {
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
                charArr[toupper(ch) - 'A']++;
            }
        }

        // ? Showing the alphabet and their repitition
        printf("Many Alphabets :\n");
        for (int i = 0; i < 26; i++) printf("%c -> %d\n", 'a' + i, charArr[i]);
        printf("--------------\n");
        
        fclose(fp);

        int temp = 0;
        for (int i = 0; i < 26; i++) {
            if (charArr[i] != 0) {
                charArrToSend[temp] = charArr[i];
                temp++;
            }
        }
        // ? Sending it to child
        write(fdFirst[1], charArrToSend, sizeof(charArrToSend));
        close(fdFirst[1]);

        wait(NULL);

        // ! B 
        close(fdSecond[1]);
        FILE *encodedFile = fopen("enc.txt", "r");
        FILE *decodedFile = fopen("dec.txt", "w");

        char huffmanTree[30][30];

        read(fdSecond[0], huffmanTree, sizeof(char) * 30 * 30);
        close(fdSecond[0]);

        // ! D
        // ? Decode tree
        decode(huffmanTree);

        // ! E
        // ? Finding the bit value after and before
        int encodedBits;
        for (int i = 0; i < 26; i++) {
            totalBytes += charArr[i];

            for (int j = 0; j < 30; j++) {
                if (huffmanTree[i][j] == '1' || huffmanTree[i][j] == '0') {
                    encodedBits++;
                } else if (huffmanTree[i][j] == '\0') {
                    j = 30;
                }
            }
        }
        totalBytes *= 8;
        
        // ? Bit value output
        printf("! TOTAL BIT !\n");
        printf("Before Huffman = %d\n", totalBytes);
        printf("After Huffman = %d\n", encodedBits);

        exit(0);
    } else {
        int temp[MAX_TREE_HT] = {0}, charArrTest[23];
        char huffmanArr[30][30], Char[23] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'W', 'Y', 'Z'};
        
        close(fdFirst[1]);

        // ? Read from parent
        read(fdFirst[0], charArrTest, sizeof(charArrTest));
        close(fdFirst[0]);

        Node *tree = buildHuffmanTree(Char, charArrTest, sizeof(Char) / sizeof(Char[0]));
        FILE *fp = fopen("file.txt", "r");
        FILE *huffmanFile = fopen("enc.txt", "w");

        if (fp == NULL) {
            printf("Can't open the txt file");
            return 1;
        }

        // ! B 
        // ? Compression process will be done here
        char ch;
        while ((ch = getc(fp)) != EOF) {
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
                ch = toupper(ch);
                printHCodes(tree, temp, 0, huffmanFile, ch);
            } else if (ch == ' ') {
                putc(' ', huffmanFile);
            } else if (ch == '\n') {
                putc('\n', huffmanFile);
            }
        }

        // ! C
        // ? De-Compression process will be done here
        for (char ch = 'A'; ch <= 'Z'; ch++) {
            saveHuffman(tree, huffmanArr, temp, 0, ch);
        }

        fclose(fp);
        fclose(huffmanFile);

        // ! D
        // ? Send to parent
        write(fdSecond[1], huffmanArr, sizeof(huffmanArr));
        close(fdSecond[1]);
        close(fdSecond[0]);

        exit(0);
    }

    return 0;
}