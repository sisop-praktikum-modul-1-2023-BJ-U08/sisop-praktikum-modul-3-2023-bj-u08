#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <errno.h>

typedef struct {
    char name[20];
    int num;
} FolderDetail;

int getWordFrequency(char* word, char* fileName) {
    char command[1000], output[1000];
    FILE* fp;
    snprintf(command, sizeof(command), "awk -v word=\"%s\" '{for(i=1;i<=NF;i++){if($i==word){count++}}}END{print count}' %s", word, fileName);

    fp = popen(command, "r");
    if (fp == NULL) {
        printf("Failed to run command\n");
        exit(EXIT_FAILURE);
    }
    fgets(output, sizeof(output), fp);
    pclose(fp);

    char* result = strdup(output);
    return atoi(result);
}

int customComparator(const void* a, const void* b) {
    const FolderDetail* ext_count_a = (const FolderDetail*)a;
    const FolderDetail* ext_count_b = (const FolderDetail*)b;
    return (int)ext_count_b->num - (int)ext_count_a->num;
}

void getFoldersName(FolderDetail* folder, char* fileName, int madeWordFrequency) {
    int idx = 0;
    char line[1024];
    FILE* fp = fopen(fileName, "r");

    char search[] = "MADE";
    char tmpExtension[madeWordFrequency][1024];

    while (fgets(line, sizeof(line), fp)) {
        if (strstr(line, search) != NULL) {
            line[strcspn(line, "\n")] = '\0';
            strcpy(tmpExtension[idx], line);
            printf("tmpExtension[%d]: %s\n", idx, tmpExtension[idx]);
            idx++;
        }
    }
    fclose(fp);

    for (int i = 0; i <= madeWordFrequency - 1; i++) {
        char* result = strstr(tmpExtension[i], search);

        result += strlen(search);

        while (*result == ' ' || *result == '\t') {
        result++;
        }

        strcpy(folder[i].name, result);
        printf("%d. Result: %s\n", i, folder[i].name);
    }
}

int main() {
    char* fileName = "log.txt";
    int banyakExtension = 8;

    int accessedWordFrequency = getWordFrequency("ACCESSED", fileName);
    printf("ACCESSED FREQUENCY : %d\n", accessedWordFrequency);

    int madeWordFrequency = getWordFrequency("MADE", fileName);
    printf("MADE FREQUENCY : %d\n", madeWordFrequency);

    FolderDetail folder[madeWordFrequency - 1];
    getFoldersName(folder, fileName, madeWordFrequency);

    int idx = 0;
    char tmpMaxFile[5];
    FolderDetail extension[banyakExtension];

    FILE* ptr = fopen("extensions.txt", "r");
    while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
        strcpy(extension[idx].name, tmpMaxFile);
        extension[idx].num = 0;
        idx++;
    }
    strcpy(extension[idx].name, "others");
    extension[idx].num = 0;
    fclose(ptr);

    for (int i = 0; i < madeWordFrequency; i++) {
        folder[i].num = 0;
        char line[1024];
        FILE* file = fopen(fileName, "r");

        while (fgets(line, 1024, file)) {
            if (strlen(folder[i].name) <= 17) {
                if (strstr(line, "MOVED") != NULL && strstr(line, folder[i].name) != NULL && strstr(line, "(") == NULL)
                folder[i].num++;
            }
            else {
                if (strstr(line, "MOVED") != NULL && strstr(line, folder[i].name) != NULL)
                folder[i].num++;
            }
        }

        if (strstr(folder[i].name, "jpg") != NULL) extension[0].num += folder[i].num;
        else if (strstr(folder[i].name, "txt") != NULL) extension[1].num += folder[i].num;
        else if (strstr(folder[i].name, "js") != NULL) extension[2].num += folder[i].num;
        else if (strstr(folder[i].name, "py") != NULL) extension[3].num += folder[i].num;
        else if (strstr(folder[i].name, "png") != NULL) extension[4].num += folder[i].num;
        else if (strstr(folder[i].name, "emc") != NULL) extension[5].num += folder[i].num;
        else if (strstr(folder[i].name, "xyz") != NULL) extension[6].num += folder[i].num;
        else if (strstr(folder[i].name, "others") != NULL) extension[7].num += folder[i].num;
    }

    qsort(folder, madeWordFrequency, sizeof(FolderDetail), customComparator);
    printf("Folder:\n");
    for (int i = madeWordFrequency - 1; i >= 0; i--) printf("%s : %d\n", folder[i].name, folder[i].num);

    qsort(extension, banyakExtension, sizeof(FolderDetail), customComparator);
    printf("Extension:\n");
    for (int i = banyakExtension - 1; i >= 0; i--) printf("%s : %d\n", extension[i].name, extension[i].num);
}
