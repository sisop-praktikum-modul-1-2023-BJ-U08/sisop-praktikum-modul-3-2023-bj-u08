#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <wait.h>

#define URL "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
#define ZIPNAME "hehe.zip";

void bash_command(char* argv[], char* command_execute) {
    pid_t id_child = fork();
    int status;

    if (id_child < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (id_child == 0) {
        printf("Child Spawned\n");
        execv(command_execute, argv);
        exit(EXIT_SUCCESS);
    }
    else {
        wait(&status);
    }
}

int main() {
    char* download_argv[] = { "wget", "--no-check-certificate", URL, "-O", ZIPNAME, NULL };
    bash_command(download_argv, "/bin/wget");
    char* unzip_argv[] = { "unzip", "-n", ZIPNAME,  NULL };
    bash_command(unzip_argv, "/bin/unzip");
}
