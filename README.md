# sisop-praktikum-modul-3-2023-BJ-U08


## Group Members


| Name | NRP |
| --- | --- |
| Mochammad Naufal Ihza Syahzada  | 5025211260 |
| Ariel Pratama Menlolo | 5025211194 |
| Teuku Aulia Azhar | 5025201142 |

## Task Check

- [x] soal1
- [x] soal2
- [ ] soal3
- [x] soal4

## Number 1 Solution

### A

```c
PARENT PROCESS
char ch;
while ((ch = fgetc(fp)) != EOF) {
    if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
        charArr[toupper(ch) - 'A']++;
    }
}

fclose(fp);

int temp = 0;
for (int i = 0; i < 26; i++) {
    if (charArr[i] != 0) {
        charArrToSend[temp] = charArr[i];
        temp++;
    }
}

write(fdFirst[1], charArrToSend, sizeof(charArrToSend));
close(fdFirst[1]);

wait(NULL);

```
explanation: Using while loop here we will get every char and store it to array by adding the value of and array index. Then we store it to another array to send it to child process using pipe.

### B

```c
CHILD PROCESS
read(fdFirst[0], charArrTest, sizeof(charArrTest));
close(fdFirst[0]);

Node *tree = buildHuffmanTree(Char, charArrTest, sizeof(Char) / sizeof(Char[0]));
FILE *fp = fopen("file.txt", "r");
FILE *huffmanFile = fopen("enc.txt", "w");

if (fp == NULL) {
    printf("Can't open the txt file");
    return 1;
}

char ch;
while ((ch = getc(fp)) != EOF) {
    if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
        ch = toupper(ch);
        printHCodes(tree, temp, 0, huffmanFile, ch);
    } else if (ch == ' ') {
        putc(' ', huffmanFile);
    } else if (ch == '\n') {
        putc('\n', huffmanFile);
    }
}
```
explanation: Compression process will be done here using while loop, it will store to a new file called enc.txt. Using Huffman algorithm that is provided in other function, it will have a 0 or 1 output.

### C

```c
CHILD PROCESS
for (char ch = 'A'; ch <= 'Z'; ch++) {
    saveHuffman(tree, huffmanArr, temp, 0, ch);
}

fclose(fp);
fclose(huffmanFile);
```
explanation: In here we called a function to change every character to Huffman code.

### D

```c
CHILD PROCESS
write(fdSecond[1], huffmanArr, sizeof(huffmanArr));
close(fdSecond[1]);
close(fdSecond[0]);

```
explanation: This code send the result of the compression to the parent process.
```c
PARENT PROCESS
char huffmanTree[30][30];
read(fdSecond[0], huffmanTree, sizeof(char) * 30 * 30);
close(fdSecond[0]);

decode(huffmanTree);
```
explanation: After reading from child using pipe, we de-compress the huffman using a function called decode();
### E
```c
PARENT PROCESS
int encodedBits;
for (int i = 0; i < 26; i++) {
    totalBytes += charArr[i];

    for (int j = 0; j < 30; j++) {
        if (huffmanTree[i][j] == '1' || huffmanTree[i][j] == '0') {
            encodedBits++;
        } else if (huffmanTree[i][j] == '\0') {
            j = 30;
        }
    }
}
totalBytes *= 8;

printf("! TOTAL BIT !\n");
printf("Before Huffman = %d\n", totalBytes);
printf("After Huffman = %d\n", encodedBits);
```
explanation: Here we will se the comparison between the actual bit (each character we assume has 8 bit) and after using the huffman algorithm. The actual bit we count it by multiplying it with 8, and for the huffman we count it briefly in each character.

### Output
- Print Output

<img width="250" src="/img/print-output.jpg">

- enc.txt
```
000111110010011 000101111000010111100 1100011010110011111110100011111100 0011110110111110101010110111111010111100 110011001010110010011 0101110110000010111101100111 111011000110000111 101100000000110001111010100 1011111010001011110100111 01101011010100001011101101011001100001011000111011000 1011011011000010111100 101100000000110001111010100 010111110111101011010100000111 0110100000000010100001101110101011011 0110010010000000101 01100110110101000000 10100000 011000110101000000 0001111111000010 1110011111100 1011001111011111011100011011000010001011101001110011111100 0000110001101011000 11000111011010000001001111010000001000111110010101 111000100011 1110001101110100111 01111000111 10110011101001111000001101111000010 001101100011001101111100110111 110011001010110010011 0101110101010110101101010001110011111100 011011110101011110110001110101111100 0101110110010110111110001110101 1010110110010101111000010 101011010011100000011000000000100111 101100000000110001111010100 10111110101001010111 01011101100101100000010000001000010 010101111001111010 01011110110001111011101111011101100111111 10110111110001110101 0101110101010110111101110001110001011101000111 10110111100 0101110110000100010110011001110011111100 101011010011100000011000000000100111 01111000111 110011001010110010011 0011110101101011001101011001011000111100 011011110100111101010000010111111100 1011011011000010111100 01011111010110101000111 0001111111000010 101110111101111010110101011111010011110111 10110111100 10101101010010111101001001100100111010110010100 10101101100101011001100000111111 1110011111100 0101110101010110101101010001110011111100 011011110101011110110001110101111100 0001111111000010 0101110110010110111110001110101 1010110110010101111000010 11000111000111111100111100 101100000000110001111010100 10110111100 01101111011000010001011001100111111100100000111111 10110111110001110101 0111100101101100110111101001000111 101011010011100000011000000000100111 01111000110010000001000101111101110111 00010111101100111 000101111000010111100 1011111101010110111110111 010111011100011010110011111110100011111100 0011110110111110101010110111111010111100 01111000111 000111111 

000101111000010111100 1100011001011011111 110011001010110010011 011001000001100011000000001100111 111001111001100 0111100101111010111001001001110101 111000100011110001110110100111101010111 11100101110100 10100111101101110011 00111101101001111000010001011111000111100 01111000110010000001000101111101110111 101011010100011010111010011001 1010110110010101111000010 1110001101110100111 01111000111
```

-dec.txt
```
YUK JANGAN LEWATKAN KESEMPATAN UNTUK MENJADI AHLI DOCKER SEJATI BERGABUNGLAH DENGAN DOCKER MASTERY BOOTCAMP FROM ZERO TO HERO YANG AKAN DISELENGGARAKAN OLEH LABORATORIUM AJK ACARA INI DIRANCANG KHUSUS UNTUK MEMBERIKAN PEMAHAMAN MENDALAM TENTANG TEKNOLOGI DOCKER SERTA MENDORONG MINAT MAHASISWA DALAM MEMPELAJARI DAN MENGGUNAKAN TEKNOLOGI INI UNTUK KEBUTUHAN PEKERJAAN DENGAN MATERI YANG SISTEMATIS DAN TERSTRUKTUR TENTUNYA AKAN MEMBERIKAN PEMAHAMAN YANG MENDALAM TENTANG LAYANAN DOCKER DAN PENGGUNAANNYA DALAM INDUSTRI TEKNOLOGI INFORMASI JADI JANGAN SAMPAI MELEWATKAN KESEMPATAN INI YA 

JANGAN LUPA UNTUK FOLLOW AKUN INSTAGRAM AJKLABITS AGAR TIDAK KETINGGALAN INFORMASI TERBARU TENTANG ACARA INI
```     



## Number 2 Solution

### A

Create a program "kalian.c" that makes a 4x2 and a 2x5 matrix then multiplies the two

Creating the matrices (Using rand to get randomised results every time we run the program)

```c
    int matrix1[ROWS_1][COLS_1];
    int matrix2[ROWS_2][COLS_2];
    int result[ROWS_1][COLS_2];

    srand(time(NULL)); 
    for (int i = 0; i < ROWS_1; i++) 
    {
        for (int j = 0; j < COLS_1; j++) 
        {
            matrix1[i][j] = rand() % 5 + 1; 
        }
    }

    srand(time(NULL));
    for (int i = 0; i < ROWS_2; i++) 
    {
        for (int j = 0; j < COLS_2; j++) 
        {
            matrix2[i][j] = rand() % 4 + 1; 
        }
    }
```
Using this function to multiply the made matrices
```c
void multiplyMatrices(int rows1, int cols1, int mat1[][cols1], int rows2, int cols2, int mat2[][cols2], int res[][cols2]) 
{
    for (int i = 0; i < rows1; i++) 
    {
        for (int j = 0; j < cols2; j++) 
        {
            int sum = 0;
            for (int k = 0; k < cols1; k++) 
            {
                sum += mat1[i][k] * mat2[k][j];
            }
            res[i][j] = sum;
        }
    }
}

Now i create a shared memory with the code 15
```c
    key_t key = ftok("shared_memory", 15);
    int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT);
    int *shmPointer = (int*) shmat(shmid, NULL, 0);
```
Lastly, Print the result and detach the shared memory
```c
for (int i = 0; i < ROWS_1; i++) 
    {
        for (int j = 0; j < COLS_2; j++) 
        {
            *(shmPointer + i * COLS_2 + j) = result[i][j];
        }
    }

    printf("Multiplication result: \n");
    printMatrix(ROWS_1, COLS_2, result);

    shmdt(shmPointer);
```

### B
Creates a new program file named Cinta.c and prints out the multiplication results form the kalian.c file

create a shared memory with the same code as 15, then after the wait we just print the result we got from the kalian.c results
```c
key_t key = ftok("shared_memory", 15);
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666 | IPC_CREAT);
    int *shm_ptr = (int*) shmat(shmid, NULL, 0);

    while (*shm_ptr == 0) 
    {
        sleep(1);
    }

    printf("Result of matrix multiplication:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%lld ", *(shm_ptr + i * COL2 + j));
        }
        printf("\n");
    }
```

### C
In cinta.c we calculate the factorials of the multiplied matrices

First off, we calculate each number's factorial form the matrix 
```c
int calculate_factorial(unsigned long long int n) 
{
    int fact = 1;
    for (unsigned long long int i = 1; i <= n; i++) 
    {
        fact *= i;
    }
    return fact;
}
```
Then we create a thread to calculate each number from the matrix multiplication result
```c
void *calculate_factorial_thread(void *arg) 
{
    int *result = (int*) arg;

    int factorial_matrix[ROW1][COL2];

    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            factorial_matrix[i][j] = calculate_factorial(result[i * COL2 + j]);
        }
    }

    printf("Factorial of the matrix:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%lld ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    pthread_exit(NULL);
}
```

### D
Makes a new program called sisop.c in which we do what we did in cinta.c but we don't use multithreading and threads

Similar to the previous code, we make a shared memory using the same code which is 15
```c
    key_t key = ftok("shared_memory", 15);
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666 | IPC_CREAT);
    int *shm_ptr = (int*) shmat(shmid, NULL, 0);

    while (*shm_ptr == 0) 
    {
        sleep(1);
    }

    unsigned long long int result_matrix[ROW1][COL2];
```
This time we multiply the matrix without the use of multithreading adn then we print it out
```c
for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            int sum = 0;
            for (unsigned long long int k = 0; k < COL2; k++) 
            {
                sum += *(shm_ptr + i * COL2 + k) * *(shm_ptr + k * COL2 + j);
            }
            result_matrix[i][j] = sum;
        }
    }

        printf("Result of matrix multiplication:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%d ", result_matrix[i][j]);
        }
        printf("\n");
    }
```
Now we calculate the matrix without the use of thread nor multithreading and print it out
```c
unsigned long long int factorial_matrix[ROW1][COL2];

    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            factorial_matrix[i][j] = factorial(*(shm_ptr + i * COL2 + j));
        }
    }

       printf("Factorial of the matrix:\n");
    for (unsigned long long int i = 0; i < ROW1; i++) 
    {
        for (unsigned long long int j = 0; j < COL2; j++) 
        {
            printf("%lld ", factorial_matrix[i][j]);
        }
        printf("\n");
    }
```
Here is the function used to calculate the multiplication
```c
int factorial(unsigned long long int n) 
{
    unsigned long long int fact = 1;
    for (unsigned long long int i = 1; i <= n; i++) 
    {
        fact *= i;
    }
    return fact;
}
```

## Number 3 Solution


## Number 4 Solution
### A
```c
#define URL "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
#define ZIPNAME "hehe.zip";

void bash_command(char* argv[], char* command_execute) {
    pid_t id_child = fork();
    int status;

    if (id_child < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (id_child == 0) {
        printf("Child Spawned\n");
        execv(command_execute, argv);
        exit(EXIT_SUCCESS);
    }
    else {
        wait(&status);
    }
}

int main() {
    char* download_argv[] = { "wget", "--no-check-certificate", URL, "-O", ZIPNAME, NULL };
    bash_command(download_argv, "/bin/wget");
    char* unzip_argv[] = { "unzip", "-n", ZIPNAME,  NULL };
    bash_command(unzip_argv, "/bin/unzip");
}
```
explanation: First we define the drive url and the zip file name, then in main we declare the argument to download and bash it. After download it we unzip it using the proper unzip argument/parameter. Same as before, then we bash command it. Bash command work using fork, more spesific the command will run in child process while the parent will wait.
### B
```c
MAIN FUNCTION
int idx = 0;
FILE* ptr = fopen("extensions.txt", "r");
while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
	strcpy(extensions[idx], tmpMaxFile);
	idx++;
}
fclose(ptr);

pthread_t tid[ExtensionTotal];

for (int i = 0; i < ExtensionTotal; i++) {
	pthread_create(&tid[i], NULL, starting_program, extensions[i]);
	int numFilePerExtension = getNumFiles(extensions[i]);
	extensionEntry[i].num = numFilePerExtension;
}

for (int i = 0; i < ExtensionTotal; i++) pthread_join(tid[i], NULL);

int numOtherFiles = getNumFiles("others");
strcpy(extensionEntry[7].name, "others");
extensionEntry[7].num = numOtherFiles;

char* mkdir_others[] = { "mkdir", "-p", "categorized/others", NULL };
bash_command(mkdir_others, "/bin/mkdir");
write_log("MADE", "categorized/others");
traverse_directory("files", "categorized/others", "others", "", 0, numOtherFiles);
```
```c
STARTING FUNCTION
char* extensionName = (char*)arg;

if      (strcmp(extensionName, "jpg") == 0) threadID = 1;
else if (strcmp(extensionName, "txt") == 0) threadID= 2;
else if (strcmp(extensionName, "js") == 0)  threadID = 3;
else if (strcmp(extensionName, "py") == 0)  threadID = 4;
else if (strcmp(extensionName, "png") == 0) threadID = 5;
else if (strcmp(extensionName, "emc") == 0) threadID = 6;
else if (strcmp(extensionName, "xyz") == 0) threadID = 7;
strcpy(extensionEntry[threadID - 1].name, extensionName);

char lowerBufExtension[5] = "";
snprintf(lowerBufExtension, sizeof(lowerBufExtension), ".%s", extensionName);

int numFilePerExtension = getNumFiles(extensionName);
for (int j = 0; j <= numFilePerExtension / 10; j++) {
		char bufDirectory[20] = "";
		snprintf(bufDirectory, sizeof(bufDirectory), "categorized/%s", extensionName);
		if (j) snprintf(bufDirectory, sizeof(bufDirectory), "categorized/%s (%d)", extensionName, j);
		printf("bufDirectory %s\n", bufDirectory);

		char upperBufExtension[5] = "";
		for (int len = 0; len < strlen(lowerBufExtension); len++) upperBufExtension[len] = toupper(lowerBufExtension[len]);

		if (!(threadID - 1)) write_log("MADE", "categorized");
		char* mkdir_argv[] = { "mkdir", "-p", bufDirectory, NULL };
		bash_command(mkdir_argv, "/bin/mkdir");
		write_log("MADE", bufDirectory);

		if (numFilePerExtension < 10) traverse_directory("files", bufDirectory, lowerBufExtension, upperBufExtension, 0, numFilePerExtension);
		else {
			if (j != numFilePerExtension / 10) traverse_directory("files", bufDirectory, lowerBufExtension, upperBufExtension, 0, 10);
			else traverse_directory("files", bufDirectory, lowerBufExtension, upperBufExtension, 0, numFilePerExtension % 10);
		}
}
```
```c
TRAVERSE FUNCTION
struct dirent* entry;
char sub_path[PATH_MAX];

DIR* dir = opendir(dir_path);
if (dir == NULL) {
	perror("Error");
	exit(1);
}

while ((entry = readdir(dir)) != NULL && cnt < maxFilePerDirectory) {
	if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
	snprintf(sub_path, PATH_MAX, "%s/%s", dir_path, entry->d_name);
	write_log("ACCESSED", sub_path);
	traverse_directory(sub_path, bufDirectory, lowerBufExtension, upperBufExtension, cnt, maxFilePerDirectory);
	}
	else if (entry->d_type == DT_REG) {
	snprintf(sub_path, PATH_MAX, "%s/%s", dir_path, entry->d_name);
	write_log("ACCESSED", sub_path);

	if (strcmp(lowerBufExtension, "others") != 0 && (strstr(entry->d_name, lowerBufExtension) != NULL || strstr(entry->d_name, upperBufExtension) != NULL)) {
		move_file(lowerBufExtension, sub_path, bufDirectory);
		cnt++;
	}
	else if (strcmp(lowerBufExtension, "others") == 0) {
		move_file(lowerBufExtension, sub_path, bufDirectory);
		cnt++;
	}
	}
}
closedir(dir);
```
```c
GET FILE NUMBER FUNCTION
char command[1000], output[1000];
FILE* fp;

char tmp[10] = "*.";
strcat(tmp, extension);
if (strcmp(extension, "others") != 0) {
	snprintf(command, sizeof(command), "find files -type f -iname \"%s\" | wc -l", tmp);
}
else snprintf(command, sizeof(command), "find files -type f | wc -l");

fp = popen(command, "r");
if (fp == NULL) {
	perror("Failed to run command");
	exit(EXIT_FAILURE);
}
fgets(output, sizeof(output), fp);
pclose(fp);

char* result = strdup(output);
return atoi(result);
```
```c
MOVE FILE FUNCTION
char mv_argv[500] = "";
snprintf(mv_argv, sizeof(mv_argv), "mv \"%s\" -t \"%s\"", src_path, dest_path);

char* mv_file[] = { "bash", "-c", mv_argv, NULL };
bash_command(mv_file, "/bin/bash");

char new_path[500] = "";
strcpy(new_path, extens);
strcat(new_path, " file : ");
strcat(new_path, src_path);
strcat(new_path, " > ");
strcat(new_path, dest_path);

write_log("MOVED", new_path);
```
```c
WRITE LOG FUNCTION
FILE* fp;
char timestamp[20];

time_t now = time(NULL);
strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&now));

fp = fopen("log.txt", "a");
fprintf(fp, "%s %s %s\n", timestamp, action, path);
fclose(fp);
```
explanation: Here is the main function, in order to answer all of the thing that the question asked we made some function for it. Just like the name of the function, it will start from the logic will start from the starting_program function.
### C
```c
qsort(extensionEntry, ExtensionTotal + 1, sizeof(ExtensionDetail), customComparator);
for (int i = ExtensionTotal; i >= 0; i--) printf("%s : %d \n", extensionEntry[i].name, extensionEntry[i].num);
```
explanation: In order to have the right output, we use qsort extension to sort the extensionEntry name and quantity. After all that we printf it using for loop.
### D
```c
pthread_t tid[ExtensionTotal];

for (int i = 0; i < ExtensionTotal; i++) {
	pthread_create(&tid[i], NULL, starting_program, extensions[i]);
	int numFilePerExtension = getNumFiles(extensions[i]);
	extensionEntry[i].num = numFilePerExtension;
}

for (int i = 0; i < ExtensionTotal; i++) pthread_join(tid[i], NULL);
```
explanation: Using this all program will be accessed using the concept of multithreading
### E
```c
void write_log(char* action, char* path) {
    FILE* fp;
    char timestamp[20];

    time_t now = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&now));

    fp = fopen("log.txt", "a");
    fprintf(fp, "%s %s %s\n", timestamp, action, path);
    fclose(fp);
}
```
explanation: This function provide us to get the right format time. Then after we got it, it will be stored in a file called log.txt. 
### F
```c
char* fileName = "log.txt";
int banyakExtension = 8;

int accessedWordFrequency = getWordFrequency("ACCESSED", fileName);
printf("ACCESSED FREQUENCY : %d\n", accessedWordFrequency);

int madeWordFrequency = getWordFrequency("MADE", fileName);
printf("MADE FREQUENCY : %d\n", madeWordFrequency);

FolderDetail folder[madeWordFrequency - 1];
getFoldersName(folder, fileName, madeWordFrequency);

int idx = 0;
char tmpMaxFile[5];
FolderDetail extension[banyakExtension];

FILE* ptr = fopen("extensions.txt", "r");
while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
	strcpy(extension[idx].name, tmpMaxFile);
	extension[idx].num = 0;
	idx++;
}
strcpy(extension[idx].name, "others");
extension[idx].num = 0;
fclose(ptr);

for (int i = 0; i < madeWordFrequency; i++) {
	folder[i].num = 0;
	char line[1024];
	FILE* file = fopen(fileName, "r");

	while (fgets(line, 1024, file)) {
		if (strlen(folder[i].name) <= 17) {
			if (strstr(line, "MOVED") != NULL && strstr(line, folder[i].name) != NULL && strstr(line, "(") == NULL)
			folder[i].num++;
		}
		else {
			if (strstr(line, "MOVED") != NULL && strstr(line, folder[i].name) != NULL)
			folder[i].num++;
		}
	}

	if (strstr(folder[i].name, "jpg") != NULL) extension[0].num += folder[i].num;
	else if (strstr(folder[i].name, "txt") != NULL) extension[1].num += folder[i].num;
	else if (strstr(folder[i].name, "js") != NULL) extension[2].num += folder[i].num;
	else if (strstr(folder[i].name, "py") != NULL) extension[3].num += folder[i].num;
	else if (strstr(folder[i].name, "png") != NULL) extension[4].num += folder[i].num;
	else if (strstr(folder[i].name, "emc") != NULL) extension[5].num += folder[i].num;
	else if (strstr(folder[i].name, "xyz") != NULL) extension[6].num += folder[i].num;
	else if (strstr(folder[i].name, "others") != NULL) extension[7].num += folder[i].num;
}

qsort(folder, madeWordFrequency, sizeof(FolderDetail), customComparator);
printf("Folder:\n");
for (int i = madeWordFrequency - 1; i >= 0; i--) printf("%s : %d\n", folder[i].name, folder[i].num);

qsort(extension, banyakExtension, sizeof(FolderDetail), customComparator);
printf("Extension:\n");
for (int i = banyakExtension - 1; i >= 0; i--) printf("%s : %d\n", extension[i].name, extension[i].num);
```
explanation: a file to check the output of log in this case are correct or not.
