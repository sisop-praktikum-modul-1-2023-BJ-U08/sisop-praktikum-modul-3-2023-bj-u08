
#include <stdio.h>
#include <sys/ipc.h>
#include <semaphore.h>
#include <string.h>
#include <fcntl.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <unistd.h>


#define TRUE 1
char json[] = "playlist.json";
sem_t* semaphore;
struct mesg_buffer {
	long mesg_type;
	char mesg_text[100];
    char cmd;
    int userId;
} message;
void play(int UID, char* name){

}
void list(){

}
void add(int UID, char* name){

}
void decrypt(){
    pid_t pid = fork();
    if (pid == 0){

    }
}

// stream call message
void streamCall(int msgid){
    do {
        msgrcv(msgid, &message, sizeof(message), 1, 0);

        if (message.cmd == 'p') {
            play(message.userId, message.mesg_text);
        } else if (message.cmd == 'l') {
            list();
        } else if (message.cmd == 'a') {
            add(message.userId, message.mesg_text);
        } else if (message.cmd == 'd') {
            decrypt();
        } else {
            printf("ERROR(!)\n");
        }

        msgctl(msgid, IPC_RMID, NULL);
    } while (TRUE);
}

void openSemaphore(){
    // semaphore section
    semaphore = sem_open("playlist", O_CREAT, 0644, 2);
    sem_init(semaphore, 1, 2);
}

int main(){
	key_t msgQueueKey;
	int msgid;

    openSemaphore();

	msgQueueKey = ftok("progfile", 65);
	msgid = msgget(msgQueueKey, 0666 | IPC_CREAT);

	streamCall(msgid);

	return 0;
}
