
#include <stdio.h>
#include <sys/msg.h>
#include <string.h>
#include <sys/ipc.h>


struct mesg_buffer {
	long mesg_type;
	char mesg_text[100];
    char cmd;
    int userId;
} message;

void userCall(char* command, int messageid, struct mesg_buffer message){
    char name[1000];

    do {
        scanf("%s", &command);
        if (!strcmp(command, "play")) {
            message.cmd = 'p';
            scanf("%[^\n]", name);
            getchar();
            strcpy(message.mesg_text, name);
        } else if (!strcmp(command, "list")) {
            message.cmd = 'l';
        } else if (!strcmp(command, "add")) {
            message.cmd = 'a';
            scanf("%[^\n]", name);
            getchar();
            strcpy(message.mesg_text, name);
        } else if (!strcmp(command, "DECRYPT")) {
            message.cmd = 'd';
        } else {
            printf("ERROR (!)\n");
            message.cmd = 'z';
        }
	    msgsnd(messageid, &message, sizeof(message), 0);
	    printf("QUEUE : %s \n", message.message_text);
    } while (!strcmp(command, "ESC"));
}

int main()
{
	key_t msgQueueKey;
	int messageid;
    char command[5];

    int UID;
    printf("User Id: ");
    scanf("%d", &UID);
	msgQueueKey = ftok("profile", 65);

	// msgget creates a message queue
	// and returns identifier
	messageid = msgget(msgQueueKey, 0666 | IPC_CREAT);
	message.mesg_type = 1;
    
    userCall(command, messageid, message);

	return 0;
}
